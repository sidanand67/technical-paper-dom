# Document Object Model (DOM)

## Introduction

DOM is an acronym for Document Object Model which is a cross-platform and language-independent interface that treats an XML or HTML document as a tree structure wherein each node is an object representing a part of the document.

<img src="dom.png" width="300px" alt="dom">

This represents the page in such a way that programs can change the document structure, style, and content as per the requirements of the programmer. With the help of this, a programmer can easily manipulate the web page at runtime thus making the web page more interactive.

The principal standardization of the DOM was handled by the World Wide Web Consortium (W3C), which last developed a recommendation in 2004.

## What is DOM?

DOM is not a programming language but an web interface that provides the ability to manipulate HTML documents. It is used in conjunction with a scripting language like Javascript. Most of the examples that you'll find on internet would likely be written in Javascript. But this must be noted that Javascript is not the only language that allows one to communicate with DOM APIs. Most recently, efforts are made to use Python in place of Javascript as the go to language for accessing DOM APIs.

The DOM is built using multiple APIs that work together. These APIs add new features and capabilities to the DOM as and when needed. For example, the HTML DOM API adds support for representing HTML documents to the core DOM, and the SVG API adds support for representing SVG documents.

## How does DOM help?

Let us take a small example to understand how DOM can make the life of a programmer easier and web pages more interactive.

```
const paragraphs = document.querySelectorAll("p");
// paragraphs[0] is the first <p> element
// paragraphs[1] is the second <p> element, etc.
alert(paragraphs[0].nodeName);
```

In the above example, DOM specifies that `querySelectorAll` method returns a list of all `<p>` elements present in the HTML document. Using this method, a programmer can easily manipulate the attributes of all the `<p>` elements in the web page without having to explicitly type out for each and every element.

It is quite evident from this example that DOM does provide a control over the entire HTML page which can be leveraged using different DOM methods that we will explore later in this article.

## Accessing the DOM

You don't need anything special to use DOM. You use the DOM API directly from within a JavaScript file called script which a program that a browser runs.

This script can be created inline within an element or externally in a JavaScript file.

### Accessing DOM with inline element

Let's take a simple example that demonstrates the use of inline use of DOM APIs.

```
<body onload="console.log('Welcome to DOM API!');">
```

As soon as the page loads, _Welcome to DOM API_ message gets printed in the console of the browser.

It is generally not a recommended practice to have both the HTML and Javascript code together in a file therefore we will look at a more complex example that solves this concern and also shows the world of possibilities with DOM.

### Accessing DOM with external element

Following is the HTML code for this example:

```html
<!doctype html>
<html>
    <head>
        <title>
            Document Object Model
        </title>
    </head>
    <body>
        <h1> Document Object Model </h1>
        <p>
            Select your color preference:
            <div>
                <label for="red">
                <input id="red" type="radio" name="color" value="Red"> Red </label>

                <label for="green">
                <input id="green" type="radio" name="color" value="Green"> Green </label>

                <label for="blue">
                <input id="blue" type="radio" name="color" value="Blue"> Blue </label>
            </div>
        </p>

        <script src="./main.js"></script>
    </body>
</html>
```

Following is the JavaScript code for this example:

```js
let headingEl = document.querySelector("h1");
let redColor = document.getElementById("red");
let greenColor = document.getElementById("green");
let blueColor = document.getElementById("blue");

redColor.addEventListener("click", () => {
    changeColor("red");
});

greenColor.addEventListener("click", () => {
    changeColor("green");
});

blueColor.addEventListener("click", () => {
    changeColor("blue");
});

let changeColor = (color) => {
    headingEl.style.color = color;
};
```

Using this example we will try to change the color of the heading using the options given below.

Following is the output of running this code:

![Output](./output.gif)

## JS DOM Helper Methods

Using JS DOM helper methods in the code makes tasks less complicated to manage. 

Following are some of the JS helper methods: 

1. `elementIndex` 
```js
export function elementIndex(el) {
    var index = 0;
    while ((el = el.previousElementSibling)) {
        index++;
    }
    return index;
}
```

2. `indexinParent`

```js
export function indexInParent(el) {
    let children = el.parentNode.childNodes;
    let num = 0;

    for (let i = 0; i < children.length; i++) {
        if (children[i] == el) return num;
        if (children[i].nodeType == 1) num++;
    }
    return -1;
}
```

3. `indexOfParent`

```js
export function indexOfParent(el) {
    return [].indexOf.call(el.parentElement.children, el);
}
```

4. `matches`

```js
export function matches(elem, selector) {
    const isMsMatch = 'msMatchesSelector' in elem && elem.msMatchesSelector(selector);
    const isMatchSelector = 'matchesSelector' in elem && elem.matchesSelector(selector)
    const isMatch = 'matches' in elem && elem.matches(selector);
    // Test the element to see if it matches the provided selector
    // use different methods for compatibility
    return isMsMatch || isMatchSelector || isMatch;
    // Return the result of the test
    // If any of the above variables is true, the return value will be true
}
```

5. `closest` 

```js
export function getClosest(elem, selector) {
    // This allows for matching based on any selector, not just a single class.
    for (; elem && elem !== document; elem = elem.parentNode) {
        // Traverse up the dom until document is reached
        if (matches(elem, selector)) {
            // Test each element to see if it matches. If it does, return it.
            return elem
        }
    }
    return null;
}

export const closest = getClosest;
```

6. `next`

```js
export function next(elem, selector) {
    if (elem.nextElementSibling) {
        if (matches(elem.nextElementSibling, selector)) {
            return elem.nextElementSibling;
        } else {
            return prev(elem.nextElementSibling, selector);
        }
    }

    return false;
}
```

Example Usage: 

```js
const element = document.querySelector('.myclass');

const nextEl = closest(element, '.mytargetclass');
```

7. `prev`

```js
export function prev(elem, selector) {
    if (elem.previousElementSibling) {
        if (matches(elem.previousElementSibling, selector)) {
            return elem.previousElementSibling;
        } else {
            return prev(elem.previousElementSibling, selector);
        }
    }

    return false;
}
```

Example Usage: 

```js
const element = document.querySelector('.myclass');

const prevEl = closest(element, '.mytargetclass');
```


## References

-   [Mozilla Developer Network: What is DOM?](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)

-   [Wikipedia: DOM](https://en.wikipedia.org/wiki/Document_Object_Model)

-   [Jim Frenette Blog: JS DOM Helper methods](https://jimfrenette.com/javascript/document/)
